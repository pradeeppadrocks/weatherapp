import { Component, EventEmitter, OnInit } from '@angular/core';
import { ApiService } from '../core/api.service';

@Component({
  selector: 'app-weather-report',
  templateUrl: './weather-report.component.html',
  styleUrls: ['./weather-report.component.scss']
})
export class WeatherReportComponent implements OnInit {
  name: string = '';
  cityData: any = [];
  showTextBox = false;
  showErrorMsg: string = '';

  constructor(
    private apiService : ApiService

  ) { }

  ngOnInit(): void {
  }
  showText(value:boolean):void{
    this.showTextBox = value
  }

  getWeather(city:string):void{
    this.getWeatherData(city);
  }

  getWeatherData(city:string):void{
    this.apiService.load(city).subscribe((data)=>{
      console.log(data);
      this.cityData = data;
      this.showTextBox = false
    }, err => {
      this.showErrorMsg = 'Data Not found'
      this.cityData = [];

    })
  }


  getIconUrl(icon: String) {
    return 'http://openweathermap.org/img/w/' + icon + ".png"
  }

}
