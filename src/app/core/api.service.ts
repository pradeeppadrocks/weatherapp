import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

let serviceUrl: String = 'https://api.openweathermap.org/data/2.5/weather'
let apiKey: String = 'b17d1b875eed6148f17aa216fdd8ebc0'
@Injectable({
  providedIn: 'root'
})
export class ApiService {


  constructor(
    private http: HttpClient,
  ) { }


  load(city: String) {
    return this.http.get(serviceUrl + '?q=' + city + '&APPID=' + apiKey)
  }
  getIconUrl(icon: String) {
    return 'http://openweathermap.org/img/w/' + icon + ".png"
  }

}
